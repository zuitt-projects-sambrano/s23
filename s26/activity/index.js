let http = require("http");
http.createServer((req, res) => {
    if(req.url === "/"){
        res.writeHead(200, {'Content-Type' : 'text/plain'})
        res.end('Hi! Welcome to B169 Booking System.')
    }else if(req.url === "/courses"){
        res.writeHead(200, {'Content-Type' : 'text/plain'})
        res.end('Welcome to the Courses Page. View our Courses.')
    }else if(req.url === "/profile"){
        res.writeHead(200, {'Content-Type' : 'text/plain'})
        res.end('Welcome to the Profile. View your details.')
    } else {
        res.writeHead(404, {'Content-Type' : 'text/plain'})
        res.end('Resource not found')
    }
}).listen(5000);
console.log('Server is running to localhost: 5000');