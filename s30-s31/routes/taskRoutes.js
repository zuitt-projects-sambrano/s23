// importing express to use the Router() method
const express = require('express');

// allows us to access our HTTP menthod routes
const router = express.Router();

// importing taskControllers
const taskControllers = require('../controllers/taskControllers');

// create a task route
router.post('/', taskControllers.createTaskController);

// retrieving all tasks route
router.get('/', taskControllers.getAllTasksController);

// retrieving single task route
router.get('/getSingleTask/:id', taskControllers.getSingleTaskController);

// updating single task's status route
router.put('/updateTaskStatus/:id', taskControllers.updateTaskStatusController);

module.exports = router;