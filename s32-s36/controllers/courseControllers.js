const bcrypt = require("bcrypt");

const Course = require("../models/Course");
const User = require("../models/User");
const router = require("../routes/userRoutes");

module.exports.addCourse = (req, res) => {

    console.log(req.body);

    let newCourse = new Course({
        
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    })

    newCourse.save()
    .then(course => res.send(course))
    .catch(err => res.send(err))
};

module.exports.getAllCourses = (req, res) => {

    Course.find({})
    .then(course => res.send(course))
    .catch(err => res.send(err))
};

module.exports.getSingleCourseController = (req, res) => {

    console.log(req.params);

    Course.findById(req.params.id)
    .then(result => res.send(result))
    .catch(error => res.send(error))
};

module.exports.archieve = (req, res) => {

	// console.log(req.course.id);
    // console.log(req.params.id);

    let updates = {
        isActive: false
    }

    Course.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(updatedCourse => res.send(updatedCourse))
    .catch(err => res.send(err))
};

module.exports.activate = (req, res) => {

	// console.log(req.course.id);
    // console.log(req.params.id);

    let updates = {
        isActive: true
    }

    Course.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(updatedCourse => res.send(updatedCourse))
    .catch(err => res.send(err))
};

module.exports.getActiveCourses = (req, res) => {

    Course.find({isActive: true})
    .then(course => res.send(course))
    .catch(err => res.send(err))
};

module.exports.updateCourse = (req, res) => {

    console.log(req.params.id);
    console.log(req.body);

    let updates = {

        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    Course.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(updatedCourse => res.send(updatedCourse))
    .catch(err => res.send(err))
};

module.exports.getInactiveCourses = (req, res) => {

    Course.find({isActive: false})
    .then(result => res.send(result))
    .catch(err => res.send(err))
};

module.exports.findCoursesByName = (req, res) => {

    Course.find({name: {$regex: req.body.name, $options: '$i'}})
    .then(result => {
        
        if(result.length === 0){
            return res.send("No courses found")
        } else {
            return res.send(result)
        }
    })
    .catch(err => res.send(err));
};

module.exports.findCoursesByPrice = (req, res) => {

    Course.find({price: req.body.price})
    .then(result => {
        
        console.log(result)
        if(result.length === 0){
            return res.send("No courses found");
        } else {
            return res.send(result);
        }
    })
    .catch(err => res.send(err));
};

module.exports.getEnrollees = (req, res) => {

    console.log(req.params.id);

    Course.findById(req.params.id)
    .then(course => res.send(course))
    .catch(error => res.send(error))
};